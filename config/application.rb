require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Longtranngoc1ProjectRails
  class Application < Rails::Application
    config.load_defaults 6.1
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.available_locales = %i[en vi]
    config.i18n.default_locale = :vi
    config.paperclip_defaults = {
      storage: :s3,
      s3_region: ENV['S3_REGION'],
      bucket: ENV['S3_BUCKET_NAME']
    }
    config.serve_static_assets = true
  end
end

Rails.application.routes.draw do
  get 'password_resets/new'
  get 'password_resets/edit'
  scope '(:locale)', locale: /en|vi/ do
    root 'static_pages#home'
    get '/contact', to: 'static_pages#contact'
    get '/signup', to: 'users#new'
    get '/news', to: 'news#index'

    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    get '/logout', to: 'sessions#destroy'

    resources :users, only: %i[new create show]
    resources :account_activations, only: :edit
    resources :password_resets, only: %i[new create edit update]
    resources :news
    resources :comments
    resources :categories
    resources :likes
    resources :like_comments
  end
end

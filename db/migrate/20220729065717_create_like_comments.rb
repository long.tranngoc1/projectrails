class CreateLikeComments < ActiveRecord::Migration[6.1]
  def change
    create_table :like_comments do |t|
      t.references :comment, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
    add_index :like_comments, %i[comment_id user_id created_at]
  end
end

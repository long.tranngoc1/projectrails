class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.string :comment
      t.references :user, foreign_key: true
      t.references :new, foreign_key: true
      t.timestamps
    end
    add_index :comments, %i[user_id new_id created_at]
  end
end

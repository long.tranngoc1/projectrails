class CreateLikes < ActiveRecord::Migration[6.1]
  def change
    create_table :likes do |t|
      t.references :user, foreign_key: true
      t.references :new, foreign_key: true
      t.timestamps
    end
    add_index :likes, %i[user_id new_id created_at]
  end
end

class CreateNews < ActiveRecord::Migration[6.1]
  def change
    create_table :news do |t|
      t.string :title
      t.string :content
      t.string :thump
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true
      t.timestamps
    end
    add_index :news, %i[user_id category_id created_at]
  end
end

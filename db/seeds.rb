User.create!(name: 'Example User',
             email: 'example@railstutorial.org',
             gender: 'Male',
             address: 'VN',
             major: 'Developer',
             age: 22,
             password: 'foobar',
             password_confirmation: 'foobar',
             activated: true,
             activated_at: Time.zone.now)
99.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  gender = 'Male'
  address = 'VN'
  major = 'Developer'
  age = 22
  password = 'password'
  User.create!(name: name,
               email: email,
               gender: gender,
               address: address,
               major: major,
               age: age,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

Category.create!(name: 'Example Category')
99.times do |n|
  name = Faker::Name.name
  Category.create!(name: name)
end

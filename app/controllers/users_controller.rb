class UsersController < ApplicationController
  before_action :find_user, only: %i[show]

  def new
    @user = User.new
  end

  def show; end

  def find_user
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:success] = t '.messages.please_check_email_to_activate'
      redirect_to @user
    else
      flash[:danger] = t '.messages.signup_fail'
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :address, :major, :gender,
                                 :password, :password_confirmation)
  end
end

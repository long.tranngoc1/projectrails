class NewsController < ApplicationController
  before_action :find_new, only: %i[edit update show destroy]
  attr_accessor :title, :content, :thump, :name, :category_id, :id

  before_action :category_all, only: %i[index edit update new create]
  before_action :user_all, only: :index

  # new
  def index
    @news_banner = New.limit(5)
    @news = if params[:key_word] || params[:category_id] || params[:user_id]
              New.where('title LIKE ?', "%#{params[:key_word]}%").where('category_id = ?', params[:category_id]).where(
                'user_id = ?', params[:user_id]
              )
            else
              New.all
            end
  end

  def new
    @new = New.new
  end

  def create
    @new = current_user.new.build(new_params)
    if @new.save
      flash[:success] = t 'news.create.created_successfully'
      redirect_to news_path
    else
      flash[:danger] = t 'news.create.created_fail'
      render 'news/new'
    end
  end

  def edit; end

  def update
    if @new_show.update(new_params)
      flash[:success] = t 'common.updated_successfully'
      redirect_to news_path
    else
      render :'news/edit'
    end
  end

  def show
    @like = Like.new
    @like_comment = LikeComment.new
    @comment = Comment.new
  end

  def destroy
    @new_show.destroy
    flash[:success] = t 'common.deleted_successfully'
    redirect_back(fallback_location: root_path)
  end

  private

  # new
  def new_params
    params.require(:new).permit(:title, :content, :thump, :category_id, :key_word, :user_id)
  end

  def find_new
    @new_show = New.find_by(id: params[:id])
  end

  def find_category(category_id)
    @category = Category.find_by(id: category_id)
  end

  def category_all
    @categories = Category.all
  end

  def user_all
    @user = User.all
  end

  # comments
  def comment_params
    params.require(:comment).permit(:comment, :new_id)
  end
end

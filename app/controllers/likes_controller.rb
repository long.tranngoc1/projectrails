class LikesController < ApplicationController
  before_action :find_like, only: %i[show destroy]
  attr_accessor :like

  def index
    @likes = Like.all
  end

  def new
    @like = Like.new
  end

  def create
    @like = Like.new(like_params)
    if @like.save
      flash[:success] = t '.new.liked_successfully'
    else
      flash[:danger] = t '.new.liked_fail'
    end
    redirect_back(fallback_location: root_path)
  end

  def show; end

  def destroy
    @like.destroy
    flash[:success] = t '.unliked_successfully'
    redirect_back(fallback_location: root_path)
  end

  private

  # new
  def like_params
    params.require(:like).permit(:new_id, :user_id)
  end

  def find_like
    @like = Like.find_by(id: params[:id])
  end
end

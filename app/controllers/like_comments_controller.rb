class LikeCommentsController < ApplicationController
  before_action :find_like_comment, only: %i[show destroy]
  attr_accessor :like_comment

  def index
    @like_comments = LikeComment.all
  end

  def new
    @like_comment = LikeComment.new
  end

  def create
    @like_comment = LikeComment.new(like_comment_params)
    if @like_comment.save
      flash[:success] = t 'likes_comment.new.liked_successfully'
    else
      flash[:danger] = t 'likes_comment.new.liked_fail'
    end
    redirect_back(fallback_location: root_path)
  end

  def show; end

  def destroy
    @like_comment.destroy
    flash[:success] = t 'likes_comment.destroy.unliked_successfully'
    redirect_back(fallback_location: root_path)
  end

  private

  # new
  def like_comment_params
    params.require(:like_comment).permit(:comment_id, :user_id)
  end

  def find_like_comment
    @like_comment = LikeComment.find_by(id: params[:id])
  end
end

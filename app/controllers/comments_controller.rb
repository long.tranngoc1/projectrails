class CommentsController < ApplicationController
  attr_accessor :comment

  before_action :find_comment, only: %i[edit update show destroy]

  def index
    @comments = Comment.all
  end

  def new
    @comment = Comment.new
  end

  def create
    @comment = current_user.comment.build(comment_params)
    if @comment.save
      flash[:success] = t 'news.create.created_successfully'
      redirect_back(fallback_location: root_path)
    else
      flash[:danger] = t 'news.create.created_fail'
      render 'comments/new'
    end
  end

  def edit; end

  def update
    if @comment.update(comment_params)
      flash[:success] = t 'common.updated_successfully'
      redirect_back(fallback_location: root_path)
    else
      render :'comments/edit'
    end
  end

  def show; end

  def destroy
    @comment.destroy
    flash[:success] = t 'common.deleted_successfully'
    redirect_back(fallback_location: root_path)
  end

  private

  def comment_params
    params.require(:comment).permit(:comment, :new_id)
  end

  def find_comment
    @comment = Comment.find(params[:id])
  end
end

class CategoriesController < ApplicationController
  attr_accessor :name

  # scope by_name order(:name)

  before_action :find_category, only: %i[show edit update destroy]

  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
    # @category.new.build
  end

  def show; end

  def create
    params[new_attributes: :user_id] = current_user.id
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = t 'common.created_successfully'
      redirect_to categories_path
    else
      flash[:danger] = t 'common.created_fail'
      render :'categories/new'
    end
  end

  def edit; end

  def update
    if @category.update(category_params)
      flash[:success] = t 'common.updated_successfully'
      redirect_to categories_path
    else
      render :'categories/edit'
    end
  end

  def destroy
    @category.destroy
    flash[:danger] = t '.deleted_fail_father' if @category.new.count.positive?
    flash[:success] = t 'common.deleted_successfully' if @category.new.count.zero?
    redirect_to categories_path
  end

  private

  def category_params
    params.require(:category).permit(:name,
                                     new_attributes: [:title, :content, :thump, { user_id: current_user.id }])
  end

  def find_category
    @category = Category.find(params[:id])
  end

end

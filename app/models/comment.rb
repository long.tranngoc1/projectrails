class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :new
  has_many :like_comments, dependent: :destroy
  scope :newest, -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :new_id, presence: true
  validates :comment, presence: true, length: { minimum: Settings.validate.comment.min_length_comment }
  after_destroy { puts 'Callback Fired' }
end

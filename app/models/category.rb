# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :new, dependent: :restrict_with_error
  accepts_nested_attributes_for :new
  validates :name, presence: true
end

class LikeComment < ApplicationRecord
  belongs_to :comment
  belongs_to :user
  after_destroy { puts 'Callback Fired' }
end

class New < ApplicationRecord
  has_many :comment, dependent: :destroy
  has_many :likes, dependent: :destroy
  belongs_to :user
  belongs_to :category
  has_one_attached :thump
  scope :newest, -> { order(created_at: :desc) }
  # validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: Settings.validate.new.max_length_title }
  validates :content, presence: true, length: { maximum: Settings.validate.new.max_length_content }
  validates :thump,
            presence: true,
            content_type: { content_type: Settings.validate.new.validate_image },
            size: { less_than: 1.megabyte }
end
